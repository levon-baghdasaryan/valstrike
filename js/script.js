$(document).ready(function() {

/*menu shows up on bars click*/
$(".bars-icon").click(function() {
 	$(".menu").toggle();
 });

// plugin for truncation texts and putting ellipsis at the end
$(".desc").succinct({size:160});
//end of truncation plugin

//Scroll to top button start
$(window).on("scroll", function() {

	var scrollBut = $("#scroll-button");
	var scrollButPos = scrollBut.offset().top;
	var aboutSecPos = $(".about").offset().top;
	var contactSecPos = $(".contact").offset().top;
	var windowPos = $(window).scrollTop();

	if (scrollButPos >= aboutSecPos && scrollButPos <= contactSecPos) {
		scrollBut.css("background-color","red");
	} else {
		scrollBut.css("background-color","#2c3e50");
	}
	if (windowPos > 50) {
		scrollBut.show();
	} else {
		scrollBut.hide();
	}

});

$("#scroll-button").click(function () {
	$("html, body").animate({scrollTop: "0"},700);
});
//Scroll to top button end	

//Scroll for navigation start
$("nav ul li").click(function() {	
	var navItemPos = $(".nav-item").eq($(this).index()).offset().top;
	var pageScrollTime = ($(this).index() * 100) + 500;	
	$("html, body").animate({scrollTop: navItemPos}, pageScrollTime);
});
//scroll for navigation end

//Slider start
var image = ["images/slide1.png", "images/slide2.png", "images/slide3.png"];
var imageNumber = 0;
var imageLength = image.length - 1;
$(".slider-right, .slider-left").click(function(num) {
	if($(".slider-right")) {
		num = 1;
	} else {
		num = -1;
	}
	imageNumber = imageNumber + num;

	if (imageNumber > imageLength){
		imageNumber = 0;
	}
	if (imageNumber < 0) {
		imageNumber = imageLength;
	}	

	$("img.slider-image").attr('src', image[imageNumber]);
	return false;
});
//Slider end

//Form Validation
$(".contact-form-info").submit(function() {
	var firstname = $(".firstname").val();  
	var email_address = $(".email_address").val();  
	var subject = $(".subject").val();  
	var textarea = $("textarea").val(); 

	if(firstname === "" || email_address === "" || subject === "" || textarea === "")	{
		alert("Please fill in all fields");
		return false;
	}
});
// end of form validation

}); //end of document ready